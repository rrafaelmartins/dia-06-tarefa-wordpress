<?php

function my_theme_scripts_function() {
    wp_enqueue_script( 'myscript', get_template_directory_uri() . '/lobinhos/adicionar-lobinho.js');
    wp_enqueue_script( 'myscript', get_template_directory_uri() . '/lobinhos/adotar.js');
    wp_enqueue_script( 'myscript', get_template_directory_uri() . '/lobinhos/home-page.js');
    wp_enqueue_script( 'myscript', get_template_directory_uri() . '/lobinhos/lista-lobinhos.js');
    wp_enqueue_script( 'myscript', get_template_directory_uri() . '/lobinhos/show-lobinho.js');
}

add_action('wp_enqueue_scripts','my_theme_scripts_function');