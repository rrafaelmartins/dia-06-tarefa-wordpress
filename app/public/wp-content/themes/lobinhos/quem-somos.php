<?php
//Template Name: quem somos
?>

<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/quem-somos.css">
<?php get_header(); ?>
    <main>
        <h1><?php the_title(); ?></h1>
        <p>
            <?php the_content(); ?>
        </p>    
    </main>
<?php get_footer(); ?>

